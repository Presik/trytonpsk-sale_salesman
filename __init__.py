# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import party
from . import invoice


def register():
    Pool.register(
        sale.Sale,
        party.Party,
        invoice.Invoice,
        invoice.PortfolioBySalesmanStart,
        # party.PortfolioDetailedStart,
        module='sale_salesman', type_='model')
    Pool.register(
        invoice.PortfolioBySalesman,
        # party.PortfolioDetailed,
        module='sale_salesman', type_='wizard')
    Pool.register(
        invoice.PortfolioBySalesmanReport,
        # party.PortfolioDetailedReport,
        module='sale_salesman', type_='report')
