# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    salesman = fields.Many2One('company.employee', 'Salesman', states={
        'readonly': Eval('state').in_(['confirmed', 'processing', 'done']),
    })

    def _get_invoice_sale(self):
        invoice = super(Sale, self)._get_invoice_sale()

        if self.salesman:
            invoice.salesman = self.salesman
            invoice.save()
        return invoice

    def on_change_party(self, name=None):
        super(Sale, self).on_change_party()
        if self.party and self.party.salesman:
            self.salesman = self.party.salesman
