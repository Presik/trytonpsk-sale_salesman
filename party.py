# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    salesman = fields.Many2One('company.employee', 'Salesman')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()

# Wilson migrate this to invoice_report
# class PortfolioDetailedStart(metaclass=PoolMeta):
#     __name__ = 'invoice_report.portfolio_detailed.start'
#     salesman = fields.Many2One('company.employee', 'Salesman')
#     grouped_by_salesman = fields.Boolean('Grouped By Salesman')
#
#
# class PortfolioDetailed(metaclass=PoolMeta):
#     'Portfolio Detailed'
#     __name__ = 'invoice_report.portfolio_detailed'
#
#     def do_print_(self, action):
#         action, data = super(PortfolioDetailed, self).do_print_(action)
#         # data['field_salesman'] = True
#         if hasattr(self.start, 'salesman') and self.start.salesman:
#             data['salesman'] = self.start.salesman.id
#         if hasattr(self.start, 'grouped_by_salesman'):
#             data['grouped_by_salesman'] = self.start.grouped_by_salesman
#         return action, data
#
#
# class PortfolioDetailedReport(metaclass=PoolMeta):
#     __name__ = 'party.portfolio_detailed.report'
#
#     @classmethod
#     def get_domain_inv(cls, dom_invoices, data):
#         domain = super(PortfolioDetailedReport, cls).get_domain_inv(dom_invoices, data)
#         if data.get('salesman'):
#             domain.append(
#                 ('salesman', 'in', data['salesman']),
#             )
#
#         return domain
